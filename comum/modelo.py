import uuid
from django.db import models
from django.utils import timezone


class ModelMixin(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    dt_criado = models.DateTimeField('Criado', auto_now_add=timezone.now, editable=False)
    dt_modificado = models.DateTimeField('Modificado', auto_now=timezone.now, editable=False)

    class Meta:
        abstract = True