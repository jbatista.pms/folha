from rest_framework import permissions


class DjangoModelPermissions(permissions.DjangoModelPermissions):
    perms_map = permissions.DjangoModelPermissions.perms_map.copy()
    perms_map.update({
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
    })