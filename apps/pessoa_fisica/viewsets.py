from rest_framework import viewsets
from .models import PessoaFisica
from .serializers import PessoaFisicaSerializer


class PessoaFisicaViewSet(viewsets.ModelViewSet):
    queryset = PessoaFisica.objects.all()
    serializer_class = PessoaFisicaSerializer