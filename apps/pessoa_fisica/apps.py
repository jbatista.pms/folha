from django.apps import AppConfig


class PessoaConfig(AppConfig):
    name = 'pessoa'
    from . import admin