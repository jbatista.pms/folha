from rest_framework import serializers
from .models import PessoaFisica


class PessoaFisicaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PessoaFisica
        fields = ['id', 'nome', 'dt_criado', 'dt_modificado']