from django.db import models
from comum.modelo import ModelMixin


class PessoaFisica(ModelMixin):
    nome = models.CharField('Nome', max_length=250)
    
    class Meta:
        db_table = 'pessoa_fisica'
        verbose_name = "Pessoa fisica"
        verbose_name_plural = "Pessoas físicas"

    def __repr__(self):
        return "<PessoaFisica(nome={nome})>".format(nome=self.nome)

    def __str__(self):
        return self.nome