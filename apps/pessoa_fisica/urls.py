from django.urls import include, path
from rest_framework import routers
from .viewsets import PessoaFisicaViewSet

router = routers.DefaultRouter()
router.register('', PessoaFisicaViewSet)
router.register('(?P<id>[/w])/', PessoaFisicaViewSet)

urlpatterns = [
    path('', include(router.urls))
]