from rest_framework import serializers
from .models import Regime


class RegimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Regime
        fields = ['id', 'nome', 'dt_criado', 'dt_modificado']