from django.apps import AppConfig


class RegimeConfig(AppConfig):
    name = 'regime'
