from rest_framework import viewsets
from .models import Regime
from .serializers import RegimeSerializer


class RegimeViewSet(viewsets.ModelViewSet):
    queryset = Regime.objects.all()
    serializer_class = RegimeSerializer