from django.db import models
from comum.modelo import ModelMixin


class Regime(ModelMixin):
    nome = models.CharField('Nome', max_length=250)

    class Meta:
        db_table = 'regime'
        verbose_name = 'Regime'
        verbose_name_plural = 'Regimes'
    
    def __str__(self):
        return self.nome
    
    def __repr__(self):
        return '<Regime(nome{nome})>'.format(nome=self.nome)