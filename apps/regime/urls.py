from django.urls import include, path
from rest_framework import routers
from .viewsets import RegimeViewSet

router = routers.DefaultRouter()
router.register('', RegimeViewSet)
router.register('(?P<id>[/w])/', RegimeViewSet)

urlpatterns = [
    path('', include(router.urls))
]