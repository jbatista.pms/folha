from django.urls import include, path
from rest_framework import routers
from .viewsets import EntidadeViewSet

router = routers.DefaultRouter()
router.register('', EntidadeViewSet)
router.register('(?P<id>[/w])/', EntidadeViewSet)

urlpatterns = [
    path('', include(router.urls))
]