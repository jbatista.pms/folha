from rest_framework import viewsets
from .models import Entidade
from .serializers import EntidadeSerializer


class EntidadeViewSet(viewsets.ModelViewSet):
    queryset = Entidade.objects.all()
    serializer_class = EntidadeSerializer