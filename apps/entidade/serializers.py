from rest_framework import serializers
from .models import Entidade


class EntidadeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entidade
        fields = ['id', 'nome', 'dt_criado', 'dt_modificado']


class EntidadeSerializerRelations(serializers.ModelSerializer):
    class Meta:
        model = Entidade
        fields = ['id', 'nome']
