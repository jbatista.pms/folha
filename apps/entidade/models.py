from django.db import models
from comum.modelo import ModelMixin 


class Entidade(ModelMixin):
    nome = models.CharField('Nome', max_length=250)

    class Meta:
        db_table = 'entidade'
        verbose_name = 'Entidade'
        verbose_name_plural = 'Entidades'
