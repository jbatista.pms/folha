from django.urls import include, path
from rest_framework import routers
from .viewsets import PessoaJuridicaViewSet

router = routers.DefaultRouter()
router.register('', PessoaJuridicaViewSet)
router.register('(?P<id>[/w])/', PessoaJuridicaViewSet)

urlpatterns = [
    path('', include(router.urls))
]