from rest_framework import serializers
from .models import PessoaJuridica


class PessoaJuridicaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PessoaJuridica
        fields = ['id', 'nome', 'dt_criado', 'dt_modificado']