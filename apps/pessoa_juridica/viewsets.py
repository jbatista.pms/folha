from rest_framework import viewsets
from .models import PessoaJuridica
from .serializers import PessoaJuridicaSerializer


class PessoaJuridicaViewSet(viewsets.ModelViewSet):
    queryset = PessoaJuridica.objects.all()
    serializer_class = PessoaJuridicaSerializer