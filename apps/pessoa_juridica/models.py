from django.db import models
from comum.modelo import ModelMixin


class PessoaJuridica(ModelMixin):
    nome = models.CharField('Nome', max_length=250)
    
    class Meta:
        db_table = 'pessoa_jurídica'
        verbose_name = "Pessoa jurídica"
        verbose_name_plural = "Pessoas jurídicas"

    def __repr__(self):
        return "<PessoaJuridica(nome={nome})>".format(nome=self.nome)

    def __str__(self):
        return self.nome