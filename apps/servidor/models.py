from django.db import models
from comum.modelo import ModelMixin


class Servidor(ModelMixin):
    centro_de_custo = models.ForeignKey(
        'centro_de_custo.CentroDeCusto', 
        related_name='servidores', 
        on_delete=models.CASCADE, 
        null=True, 
        blank=False,
    )
    entidade = models.ForeignKey(
        'entidade.Entidade', 
        related_name='servidores', 
        on_delete=models.CASCADE,
        null=True, 
        blank=False,
    )
    pessoa = models.ForeignKey(
        'pessoa_fisica.PessoaFisica', 
        on_delete=models.CASCADE, 
        related_name='vinculos'
    )

    class Meta:
        verbose_name = 'Servidor'
        verbose_name_plural = 'Servidores'

    def __repr__(self):
        return "<Servidor(nome={nome})>".format(nome=self.pessoa.nome)

    def __str__(self):
        return self.pessoa.nome