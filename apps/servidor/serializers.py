from rest_framework import serializers
from apps.centro_de_custo.models import CentroDeCusto
from apps.centro_de_custo.serializers import CentroDeCustoSerializer
from apps.entidade.models import Entidade
from apps.entidade.serializers import EntidadeSerializerRelations
from apps.pessoa_fisica.models import PessoaFisica
from apps.pessoa_fisica.serializers import PessoaFisicaSerializer
from .models import Servidor


class ServidorSerializer(serializers.ModelSerializer):
    centro_de_custo = CentroDeCustoSerializer(read_only=True)
    centro_de_custo_id = serializers.PrimaryKeyRelatedField(
        queryset=CentroDeCusto.objects.all(),
        source='centro_de_custo',
        write_only=True,
    )
    entidade = EntidadeSerializerRelations(read_only=True)
    entidade_id = serializers.PrimaryKeyRelatedField(
        queryset=Entidade.objects.all(),
        source='entidade',
        write_only=True,
    )
    pessoa = PessoaFisicaSerializer(read_only=True)
    pessoa_id = serializers.PrimaryKeyRelatedField(
        queryset=PessoaFisica.objects.all(),
        source='pessoa',
        write_only=True,
    )

    class Meta:
        model = Servidor
        fields = [
            'id', 
            'centro_de_custo', 
            'centro_de_custo_id',
            'entidade', 
            'entidade_id',
            'pessoa', 
            'pessoa_id', 
            'dt_criado', 
            'dt_modificado'
        ]
    
    def validate(self, attrs):
        if attrs['centro_de_custo'].entidade != attrs['entidade']:
            raise serializers.ValidationError(
                "Centro de custo '{cc}' não pertence a entidade '{entidade}'".format(
                    cc=attrs['centro_de_custo'].nome,
                    entidade=attrs['entidade'].nome,
                )
            )
        return attrs