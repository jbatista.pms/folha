from django.core.exceptions import ValidationError
from rest_framework import viewsets, response

from apps.pessoa_fisica.models import PessoaFisica
from .models import Servidor
from .serializers import ServidorSerializer


class ServidorViewSet(viewsets.ModelViewSet):
    queryset = Servidor.objects.all()
    serializer_class = ServidorSerializer