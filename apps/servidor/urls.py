from django.urls import include, path
from rest_framework import routers
from .viewsets import ServidorViewSet

router = routers.DefaultRouter()
router.register('', ServidorViewSet)

urlpatterns = [
    path('', include(router.urls))
]