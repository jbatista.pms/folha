from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

urlpatterns = [
    path('centro_de_custo/', include('apps.centro_de_custo.urls')),
    path('entidade/', include('apps.entidade.urls')),
    path('pessoa_fisica/', include('apps.pessoa_fisica.urls')),
    path('pessoa_juridica/', include('apps.pessoa_juridica.urls')),
    path('regime/', include('apps.regime.urls')),
    path('servidor/', include('apps.servidor.urls')),
    path('auth/', include('apps.auth.urls'), name='auth'),
    path('auth/', include('djoser.urls'), name='djoser'),
    path('token/', TokenObtainPairView.as_view(), name='token'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]