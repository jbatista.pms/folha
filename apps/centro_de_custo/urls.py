from django.urls import include, path
from rest_framework import routers
from .viewsets import CentroDeCustoViewSet

router = routers.DefaultRouter()
router.register('', CentroDeCustoViewSet)
router.register('(?P<id>[/w])/', CentroDeCustoViewSet)

urlpatterns = [
    path('', include(router.urls))
]