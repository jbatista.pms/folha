from rest_framework import viewsets
from .models import CentroDeCusto
from .serializers import CentroDeCustoSerializer


class CentroDeCustoViewSet(viewsets.ModelViewSet):
    queryset = CentroDeCusto.objects.all()
    serializer_class = CentroDeCustoSerializer