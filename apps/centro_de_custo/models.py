from django.db import models
from comum.modelo import ModelMixin


class CentroDeCusto(ModelMixin):
    entidade = models.ForeignKey(
        'entidade.Entidade', 
        related_name='centros_de_custo',
        on_delete=models.CASCADE,
        null=True,
        blank=False,
    )
    nome = models.CharField('Nome', max_length=250)

    class Meta:
        db_table = 'centro_de_custo'
        verbose_name = 'Centro de custo'
        verbose_name_plural = 'Centros de custo'
    
    def __str__(self):
        return self.nome
    
    def __repr__(self):
        return '<CentroDeCusto(nome{nome})>'.format(nome=self.nome)