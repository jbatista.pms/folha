from rest_framework import serializers
from apps.entidade.models import Entidade 
from apps.entidade.serializers import EntidadeSerializer
from .models import CentroDeCusto


class CentroDeCustoSerializer(serializers.ModelSerializer):
    entidade = EntidadeSerializer(read_only=True)
    entidade_id = serializers.PrimaryKeyRelatedField(
        queryset=Entidade.objects.all(),
        source='entidade',
        write_only=True,
    )

    class Meta:
        model = CentroDeCusto
        fields = [
            'id', 
            'entidade', 
            'entidade_id',  
            'nome', 
            'dt_criado', 
            'dt_modificado'
        ]